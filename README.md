# Authentication Express and Passport.js



## Getting started

1. Install Project: https://gitlab.com/adrmulyawan/authentication-express-and-passport.js.git
2. Jalankan Perintah Install: npm install
3. Buat file .env untuk menyimpan PORT, SECRET_KEY, dan SALT_ROUND
4. Untuk Menjalankan:
- [ ] nodemon server-local.js (Untuk Menjalankan Authentication Dengan Session-Cookie)
- [ ] nodemon server-jwt.js (Untuk Menjalankan Authentication Dengan JWT)


## Features

1. Authentication Using Passport Local (Session-Cookie)
2. Authentication Using Passport JWT (JSON Web Token)

## Authors 
Adrian Mulyawan

## Project status
Simple Project for Learn
