require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const passport = require('./app/lib/passport-jwt');
const router = require('./app/routes/route-jwt');

const app = express();
const port = +process.env.PORT;

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

// > Use passport
app.use(passport.initialize());

app.use(router);

app.listen(port, () => {
  console.info(`${new Date()} | Express run in port ${port}`);
});