require('dotenv').config();
const express = require('express');
const session = require('express-session');
const flash = require('express-flash');
const morgan = require('morgan');
const passport = require('./app/lib/passport-local');
const router = require('./app/routes/route-local');

const app = express();
const port = +process.env.PORT;

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

app.use(session({
  secret: process.env.SECRET_KEY,
  resave: false,
  saveUninitialized: false
}));

// > Use passport-local.js
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

// > Set View Engine Using EJS
app.set('view engine', 'ejs');

app.use(router);

app.listen(port, () => {
  console.info(`${new Date()} | Express run in port ${port}`);
});