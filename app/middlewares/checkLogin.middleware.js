const isLogin = (req, res, next) => {
  // > req.user didapat dari user yang telah login
  if (req.user) {
    // > Jika user telah login direct kehalaman / (home)
    return res.redirect('/');
  }

  // > Jika belum login arahkan ke requestnya
  return next();
};

module.exports = {
  isLogin
};