const User = require('../db/models').User;

const checkDuplicateUsername = async (req, res, next) => {
  try {
    const checkUsername = await User.findOne({
      where: {
        username: req.body.username
      }
    });

    if (checkUsername) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: 'Sorry! Username is Already Exists'
      });
    }

    next();
  } catch (error) {
    console.info(error);
  }
};

module.exports = {
  checkDuplicateUsername
};