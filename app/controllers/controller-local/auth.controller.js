require('dotenv').config();
const models = require('../../db/models');
const User = models.User;
const Profile = models.Profile;
const bcrypt = require('bcrypt');
const saltRounds = +process.env.SALT_ROUND;

class AuthController {
  // > Method Untuk Render Halaman Register
  static renderRegister = (req, res) => {
    return res.render('register', {
      title: 'Register'
    });
  };

  // > Method Untuk Register User 
  static registerProcess = async (req, res) => {
    try {
      // > Tangkap Username dan Password
      const { username, password } = req.body;
      
      // > Format password menggunakan bcrypt
      const bcryptPassword = await bcrypt.hash(password, saltRounds);

      await User.create({
        username,
        password: bcryptPassword,
        profile: {
          fullname: 'Nama Belum Diatur',
          email: 'Email Belum Diatur',
          address: 'Alamat Belum Diatur',
          phone_number: 'Ponsel Belum Diatur',
        }
      }, { include: ['profile'] });

      return res.redirect('/login');
    } catch (error) {
      console.info(error);
    }
  }

  // > Method Untuk Menampilkan Halaman Login
  static renderLogin = (req, res) => {
    return res.render('login', {
      title: 'Login'
    });
  };

  // > Method Untuk Login User
  static loginProcess = async (username, password) => {
    try {
      const userDatabase = await User.findOne({
        where: {
          username
        }
      });

      if (!userDatabase) {
        console.info('Username / Password Salah YGY!');
        return Promise.reject('Username Not Found!');
      }

      const checkPassword = await bcrypt.compare(password, userDatabase.password);

      if (!checkPassword) {
        console.info('Password Salah YGY!');
        return Promise.reject('Wrong Password!');
      }

      return Promise.resolve(userDatabase);
    } catch (error) {
      console.info(error);
    }
  };

  // > Method Untuk Melihat Profile
  static whoAmI = async (req, res) => {
    const username = req.user.dataValues.username;

    const profile = await Profile.findOne({
      where: {
        user_id: req.user.dataValues.id
      }
    });

    return res.render('profile', {
      username,
      profile
    });
  }

  // > Method Logout
  static logout = (req, res) => {
    req.session.destroy(function(err) {
      res.redirect('/login');
    });
  }
}

module.exports = {
  AuthController
}