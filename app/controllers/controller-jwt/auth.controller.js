require('dotenv').config();
const models = require('../../db/models');
const User = models.User;
const Profile = models.Profile;
const bcrypt = require('bcrypt');
const saltRounds = +process.env.SALT_ROUND;
const jwt = require('jsonwebtoken');

class AuthController {
  // > Buat method handle register
  static userRegister = async(req, res) => {
    try {
      const { username, password } = req.body;
      const encryptPassword = await bcrypt.hash(password, saltRounds);

      // > Membuat user baru beserta profile
      // => profile (refrences ke table profile)
      const user = await User.create({
        username,
        password: encryptPassword,
        profile: req.body.profile
      }, {
        include: ['profile']
      });

      return res.status(201).json({
        status: 'Success',
        statusCode: 201,
        message: 'Register is Success!',
        data: user
    });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: 'Error Found!',
        error: error
      });
    }
  };

  // > Method Untuk Generate Token
  static generateToken = (id, username) => {
    // > Tangkap id dan username
    const payload = {
      id,
      username
    };

    // > SECRET_KEY Dari .env file
    const SECRET_KEY = process.env.SECRET_KEY;

    // > Sign JWT
    // => JWT Expired Dalam 24 Jam
    const token = jwt.sign(payload, SECRET_KEY, {
      expiresIn: 86400
    });

    return token;
  };

  static userLogin = async (username, password) => {
    try {
      const userDatabase = await User.findOne({
        where: {
          username
        }
      });

      if (!userDatabase) {
        console.info('User or Password Wrong!');
        return Promise.reject('Check Again Username or Password!')
      }

      const checkPassword = await bcrypt.compare(password, userDatabase.password);

      if (!checkPassword) {
        console.info('Wrong Password');
        return Promise.reject('Wrong Password!');
      }

      return Promise.resolve(userDatabase);
    } catch (error) {
      return Promise.reject(error);
    }
  };

  static loginWithJWT = async (req, res) => {
    try {
      // > Tangkap Username dan Password (Dari body)
      const { username, password } = req.body;

      // > Check Login
      const checkLogin = await AuthController.userLogin(username, password);

      // > Simpan id dan username user
      const userId = checkLogin.dataValues.id;
      const userUsername = checkLogin.dataValues.username;

      // > Simpan id dan username user ke token payload
      const generateToken = AuthController.generateToken(userId, userUsername);

      return res.status(200).json({
        status: 'Success',
        statusCode: 200,
        message: 'Success Login!',
        accessToken: generateToken
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: 'Error Found!',
        error: error
      })
    }
  };

  static whoAmI = (req, res) => {
    const {id, username} = req.user;
    res.status(200).json({
      status: 'Success',
      statusCode: 200,
      user: {
        id, 
        username
      }
    });
  }

  static showProfile = async (req, res) => {
    try {
      console.info(req.user);

      const profile = await Profile.findOne({
        where: {
          user_id: req.user.id
        }
      });

      return res.status(200).json({
        status: 'Success',
        statusCode: 200,
        message: 'Data Profile Found!',
        data: profile
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: 'Error Found!',
        error: error
      });
    }
  };
}

module.exports = {
  AuthController
};