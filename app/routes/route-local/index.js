const express = require('express');
const router = express.Router();
const passport = require('../../lib/passport-local');

// > Controller
const { AuthController } = require('../../controllers/controller-local/auth.controller');

// > Middleware
const CheckUsername = require('../../middlewares/checkDupilcateUsername.middleware');
const isRestricted = require('../../middlewares/localRestrict.middleware');
const CheckLogin = require('../../middlewares/checkLogin.middleware');

router.get('/', [
  isRestricted.isAuthenticated
], (req, res) => {
  res.render('index', {
    title: 'Home'
  });
});

router.get('/register', [
  CheckLogin.isLogin
], AuthController.renderRegister);
router.post('/register', [
  CheckUsername.checkDuplicateUsername
], AuthController.registerProcess);

router.get('/login', [
  CheckLogin.isLogin
], AuthController.renderLogin);
router.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash: true
}));

router.get('/whoami', [
  isRestricted.isAuthenticated
], AuthController.whoAmI);

router.get('/logout', AuthController.logout);

module.exports = router;