const router = require('express').Router();

// > Controllers
const { AuthController } = require('../../controllers/controller-jwt/auth.controller');

// > Middleware
const CheckUsername = require('../../middlewares/checkDupilcateUsername.middleware');
const CheckAuthenticated = require('../../middlewares/jwtRestrict.middleware');

router.get('/api/v1/hello', (req, res) => {
  return res.status(200).json({
    status: 'Success',
    statusCode: 200,
    message: 'Hello World!'
  });
});

router.post('/api/v1/register', [
  CheckUsername.checkDuplicateUsername
], AuthController.userRegister);

router.post('/api/v1/login', AuthController.loginWithJWT);

router.get('/api/v1/whoami', [
  CheckAuthenticated.isAuthenticated
], AuthController.whoAmI);

router.get('/api/v1/profile', [
  CheckAuthenticated.isAuthenticated
], AuthController.showProfile);

module.exports = router;