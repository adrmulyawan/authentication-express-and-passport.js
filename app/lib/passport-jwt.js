require('dotenv').config();
const passport = require('passport');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const User = require('../db/models').User;

// > Verify JWT
const options = {
  // > Untuk mengekstrak JWT dari request, dan mengambil token dari header
  // => yang bernama 'authorization'
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),

  // > Secret Key
  secretOrKey: process.env.SECRET_KEY
};

passport.use(new JwtStrategy(options, async (payload, done) => {
  // > Payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukan di parameter pertama dari JWT
  User.findByPk(payload.id)
    .then(user => done(null, user))
    .catch(err => done(err, false));
}));

module.exports = passport;